<?php
/**
 * Created by PhpStorm.
 * User: Robert Cuzencu
 * Date: 04.10.2016
 * Time: 17:20
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="fotos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FotosRepository")
 */

class Fotos
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $fileName;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $smallFotoFileName;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSmallFotoFileName()
    {
        return $this->smallFotoFileName;
    }

    /**
     * @param mixed $smallFotoFileName
     */
    public function setSmallFotoFileName($smallFotoFileName)
    {
        $this->smallFotoFileName = $smallFotoFileName;

        return $this;
    }
}