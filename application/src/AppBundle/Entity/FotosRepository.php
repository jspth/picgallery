<?php
/**
 * Created by PhpStorm.
 * User: Robert Cuzencu
 * Date: 05.10.2016
 * Time: 13:24
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;


class FotosRepository extends EntityRepository
{
    public function loadFotosByFileName($fotosBatch)
    {
        $query = $this->createQueryBuilder('f')
            ->orderBy('f.name', 'ASC');
        return $query
            ->setFirstResult($fotosBatch)
            ->setMaxResults(30)
            ->getQuery()
            ->execute();
    }

    public function loadAllFotos()
    {
        return $this->getEntityManager()
            ->createQuery(

            'SELECT f
            FROM AppBundle:Fotos f '
        )->getResult();
    }
}

/*
 * <script type='text/javascript' src="{{ asset('unitegallery/js/unitegallery.min.js') }}"></script>
    <link rel='stylesheet' href="{{ asset('unitegallery/css/unite-gallery.css') }}" type='text/css'/>
    <script type='text/javascript' src="{{ asset('unitegallery/themes/tiles/ug-theme-tiles.js') }}"></script>
    <link rel='stylesheet' href="{{ asset('unitegallery/themes/default/ug-theme-default.css') }}" type='text/css' />
 */